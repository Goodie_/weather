CREATE TABLE forecast (
longitude double precision,
latitude double precision,
datetime datetime,
lowCloudLayer double,
middleCloudLayer double,
highCloudLayer double,
entireAtmosphere double,
connectiveCloudLayer double,
boundaryCloudLayer double,
landCover double,
iceCover double
);

CREATE UNIQUE INDEX longitude_latitude ON forecast(longitude, latitude, datetime);