package org.goodiemania.weather.database;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.flywaydb.core.Flyway;
import org.goodiemania.weather.domain.ForecastStorage;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.PreparedBatch;
import org.sqlite.SQLiteDataSource;

public class Datasource {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm");
    private static final DecimalFormat df = new DecimalFormat("##.##");

    private final Jdbi dbi;
    private final Handle handleByAnotherName;

    static {
        df.setRoundingMode(RoundingMode.HALF_UP);
    }

    public Datasource() {
        final SQLiteDataSource ds = new SQLiteDataSource();
        ds.setUrl("jdbc:sqlite:tmpData.db");
        dbi = Jdbi.create(ds);
        handleByAnotherName = dbi.open();

        Flyway flyway = Flyway.configure()
                .dataSource(ds)
                .load();

        int migrate = flyway.migrate();
        System.out.println("Ran Flyway migration: " + migrate);
    }

    public void save(final List<ForecastStorage> storageList) {
        saveOld(storageList);
    }

    public int[] saveNew(final List<ForecastStorage> storageList) {
        PreparedBatch batch = handleByAnotherName.prepareBatch("insert or replace into forecast "
                + "(longitude,\n"
                + "latitude,\n"
                + "datetime,\n"
                + "lowCloudLayer,\n"
                + "middleCloudLayer,\n"
                + "highCloudLayer,\n"
                + "entireAtmosphere,\n"
                + "connectiveCloudLayer,\n"
                + "boundaryCloudLayer,\n"
                + "landCover,\n"
                + "iceCover) "
                + "values "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        storageList.forEach(storage -> {
            batch.add(
                    df.format(storage.getLongitude()),
                    df.format(storage.getLatitude()),
                    dateTimeFormatter.format(storage.getDatetime()),
                    storage.getLowCloudLayer(),
                    storage.getMiddleCloudLayer(),
                    storage.getHighCloudLayer(),
                    storage.getEntireAtmosphere(),
                    storage.getConnectiveCloudLayer(),
                    storage.getBoundaryCloudLayer(),
                    storage.getLandCover(),
                    storage.getIceCover());
        });

        return batch.execute();
    }

    public void saveOld(final List<ForecastStorage> storageList) {
        dbi.useHandle(handle -> {
            handle.execute("BEGIN TRANSACTION;");
            storageList.forEach(storage -> {
                handle.execute("insert or replace into forecast "
                                + "(longitude,\n"
                                + "latitude,\n"
                                + "datetime,\n"
                                + "lowCloudLayer,\n"
                                + "middleCloudLayer,\n"
                                + "highCloudLayer,\n"
                                + "entireAtmosphere,\n"
                                + "connectiveCloudLayer,\n"
                                + "boundaryCloudLayer,\n"
                                + "landCover,\n"
                                + "iceCover) "
                                + "values "
                                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        df.format(storage.getLongitude()),
                        df.format(storage.getLatitude()),
                        dateTimeFormatter.format(storage.getDatetime()),
                        storage.getLowCloudLayer(),
                        storage.getMiddleCloudLayer(),
                        storage.getHighCloudLayer(),
                        storage.getEntireAtmosphere(),
                        storage.getConnectiveCloudLayer(),
                        storage.getBoundaryCloudLayer(),
                        storage.getLandCover(),
                        storage.getIceCover()
                );
            });
            handle.execute("COMMIT TRANSACTION;");
        });
    }

    public static void main(String[] args) {
        System.out.println(dateTimeFormatter.format(ZonedDateTime.now(ZoneId.of("UTC"))));
        System.out.println(ZonedDateTime.now().getZone());
    }

    public List<ForecastStorage> findByDate(final ZonedDateTime now, final String latatude, final String longitude) {
        return dbi.withHandle(handle -> handle.createQuery("select * from forecast where datetime < ?")
                .bind(0, dateTimeFormatter.format(now))
                .mapToBean(ForecastStorage.class)
                .list());
    }
}
