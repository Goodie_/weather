package org.goodiemania.weather;

import io.javalin.Javalin;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.goodiemania.weather.database.Datasource;
import org.goodiemania.weather.http.impl.HttpRequestServiceImpl;
import org.goodiemania.weather.internal.ReportDateGenerator;
import org.goodiemania.weather.internal.WeatherProcessor;
import org.goodiemania.weather.internal.forecast.ForecastData;
import org.goodiemania.weather.internal.forecast.GFSService;
import org.goodiemania.weather.internal.forecast.RecordSelection;
import org.goodiemania.weather.internal.requester.FileRequestResponse;
import org.goodiemania.weather.internal.requester.FileRequester;
import org.goodiemania.weather.internal.requester.FileRequesterImpl;

public class Main {
    public static void main(String[] args) {
        Set<RecordSelection> selections = Set.of(
                RecordSelection.LOW_CLOUD_LAYER,
                RecordSelection.MIDDLE_CLOUD_LAYER,
                RecordSelection.HIGH_CLOUD_LAYER,
                RecordSelection.ENTIRE_ATMOSPHERE,
                RecordSelection.CONNECTIVE_CLOUD_LAYER,
                RecordSelection.BOUNDARY_CLOUD_LAYER,
                RecordSelection.LAND_COVER,
                RecordSelection.ICE_COVER
        );
        final ZoneId globalZone = ZoneId.of("UTC");
        final ZoneId localZone = ZoneId.of("Pacific/Auckland");

        Datasource datasource = new Datasource();
        GFSService gfsService = new GFSService(selections);
        ReportDateGenerator reportDateGenerator = new ReportDateGenerator(globalZone);

        HttpRequestServiceImpl httpRequestService = new HttpRequestServiceImpl();
        FileRequester fileRequester = new FileRequesterImpl(httpRequestService);
        //FileRequester fileRequester = new MockLocalFile();

        WeatherProcessor weatherProcessor = new WeatherProcessor(datasource);

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(() -> {
            ZonedDateTime requestTime = reportDateGenerator.get();
            System.out.println("Starting download; " + ZonedDateTime.now());
            System.out.println("Downloading for start time; " + requestTime);
            List<FileRequestResponse> fileResponses = IntStream.range(1, 12)
                    .parallel()
                    .mapToObj(requestTime::plusHours)
                    .map(forecastTime -> fileRequester.request(requestTime, forecastTime))
                    .collect(Collectors.toList());

            System.out.println("Starting process; " + ZonedDateTime.now());

            List<ForecastData> collect = fileResponses.stream()
                    .map(gfsService::processFile)
                    .flatMap(Optional::stream)
                    .collect(Collectors.toList());

            collect.forEach(forecastData -> {
                System.out.println(forecastData.getForecastTime().withZoneSameInstant(localZone));
                System.out.println("------");
            });

            collect.forEach(weatherProcessor::store);
            collect.forEach(weatherProcessor::letsMakeImage);

            System.out.println("Finished; " + ZonedDateTime.now());
        }, 0, 8, TimeUnit.HOURS);

        Javalin app = Javalin.create().start(7000);
        app.get("/check", ctx -> ctx.result("Hello World"));
        app.get("/report/:latatude/:longitude", ctx -> {
            datasource.findByDate(ZonedDateTime.now(globalZone), ctx.pathParam("latatude"), ctx.pathParam("longitude"));
        });
    }
}


