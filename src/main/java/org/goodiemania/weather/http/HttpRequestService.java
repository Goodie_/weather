package org.goodiemania.weather.http;

public interface HttpRequestService {
    HttpServiceResponse get(String uriString, final boolean cachedResponseAllowed);
}
