package org.goodiemania.weather.http.impl;

import java.io.InputStream;
import java.time.ZonedDateTime;
import org.goodiemania.weather.http.HttpServiceResponse;
import org.goodiemania.weather.http.ResponseType;

public class HttpServiceResponseImpl implements HttpServiceResponse {
    private final InputStream response;
    private final int status;
    private final ZonedDateTime requestTime;

    private HttpServiceResponseImpl(
            final InputStream response,
            final int status,
            final ZonedDateTime requestTime) {
        this.response = response;
        this.status = status;
        this.requestTime = requestTime;
    }

    public static HttpServiceResponse of(final InputStream response, final int status, final ZonedDateTime requestTime) {
        return new HttpServiceResponseImpl(response, status, requestTime);
    }

    @Override
    public InputStream getResponse() {
        return response;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public ZonedDateTime getRequestTime() {
        return requestTime;
    }
}
