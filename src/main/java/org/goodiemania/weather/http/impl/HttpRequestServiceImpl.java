package org.goodiemania.weather.http.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.ZonedDateTime;
import org.goodiemania.weather.http.HttpServiceResponse;

public class HttpRequestServiceImpl {
    private final HttpClient httpClient;

    public HttpRequestServiceImpl() {
        httpClient = HttpClient.newBuilder().build();
    }

    /**
     * Fetches the content at a specific URL.
     *
     * @param uriString The URL to make a (GET) request too.
     * @return The string value of the page without any processing done to it.
     */
    public HttpServiceResponse get(final String uriString) {
        URI uri = URI.create(uriString);
        HttpRequest request = HttpRequest.newBuilder().GET()
                .uri(uri)
                .setHeader("User-Agent", "BookInformation Java bot: https://gitlab.com/Goodie_/BookInformation")
                .build();

        try {
            HttpResponse<InputStream> response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());

            return HttpServiceResponseImpl.of(
                    response.body(),
                    response.statusCode(),
                    ZonedDateTime.now());
        } catch (IOException | InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
