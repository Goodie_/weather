package org.goodiemania.weather.http;

import java.io.InputStream;
import java.time.ZonedDateTime;

public interface HttpServiceResponse {
    InputStream getResponse();

    int getStatus();

    ZonedDateTime getRequestTime();
}
