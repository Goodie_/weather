package org.goodiemania.weather.http;

public enum ResponseType {
    LIVE,
    CACHED,
    TEST
}
