package org.goodiemania.weather.domain;

import java.time.ZonedDateTime;

public class ForecastStorage {
    private double latitude;
    private double longitude;
    private ZonedDateTime datetime;
    private Float lowCloudLayer;
    private Float middleCloudLayer;
    private Float highCloudLayer;
    private Float entireAtmosphere;
    private Float connectiveCloudLayer;
    private Float boundaryCloudLayer;
    private Float landCover;
    private Float iceCover;

    public ForecastStorage() {
    }

    public ForecastStorage(
            final double latitude,
            final double longitude,
            final ZonedDateTime datetime,
            final Float lowCloudLayer,
            final Float middleCloudLayer,
            final Float highCloudLayer,
            final Float entireAtmosphere,
            final Float connectiveCloudLayer,
            final Float boundaryCloudLayer,
            final Float landCover,
            final Float iceCover) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.datetime = datetime;
        this.lowCloudLayer = lowCloudLayer;
        this.middleCloudLayer = middleCloudLayer;
        this.highCloudLayer = highCloudLayer;
        this.entireAtmosphere = entireAtmosphere;
        this.connectiveCloudLayer = connectiveCloudLayer;
        this.boundaryCloudLayer = boundaryCloudLayer;
        this.landCover = landCover;
        this.iceCover = iceCover;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    public ZonedDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(final ZonedDateTime datetime) {
        this.datetime = datetime;
    }

    public Float getLowCloudLayer() {
        return lowCloudLayer;
    }

    public void setLowCloudLayer(final Float lowCloudLayer) {
        this.lowCloudLayer = lowCloudLayer;
    }

    public Float getMiddleCloudLayer() {
        return middleCloudLayer;
    }

    public void setMiddleCloudLayer(final Float middleCloudLayer) {
        this.middleCloudLayer = middleCloudLayer;
    }

    public Float getHighCloudLayer() {
        return highCloudLayer;
    }

    public void setHighCloudLayer(final Float highCloudLayer) {
        this.highCloudLayer = highCloudLayer;
    }

    public Float getEntireAtmosphere() {
        return entireAtmosphere;
    }

    public void setEntireAtmosphere(final Float entireAtmosphere) {
        this.entireAtmosphere = entireAtmosphere;
    }

    public Float getConnectiveCloudLayer() {
        return connectiveCloudLayer;
    }

    public void setConnectiveCloudLayer(final Float connectiveCloudLayer) {
        this.connectiveCloudLayer = connectiveCloudLayer;
    }

    public Float getBoundaryCloudLayer() {
        return boundaryCloudLayer;
    }

    public void setBoundaryCloudLayer(final Float boundaryCloudLayer) {
        this.boundaryCloudLayer = boundaryCloudLayer;
    }

    public Float getLandCover() {
        return landCover;
    }

    public void setLandCover(final Float landCover) {
        this.landCover = landCover;
    }

    public Float getIceCover() {
        return iceCover;
    }

    public void setIceCover(final Float iceCover) {
        this.iceCover = iceCover;
    }
}
