package org.goodiemania.weather.domain;

import java.time.ZonedDateTime;

public class GFSData {
    private final String parameterName;
    private final String levelName;
    private final ZonedDateTime forecastTime;
    private final float originX;
    private final float originY;
    private final float deltaX;
    private final float deltaY;
    private final int noXPoints;
    private final int noYPoints;
    private final float[] data;

    public GFSData(
            final String parameterName,
            final String levelName,
            final ZonedDateTime forecastTime,
            final float originX,
            final float originY,
            final float deltaX,
            final float deltaY,
            final int noXPoints,
            final int noYPoints,
            final float[] data) {
        this.parameterName = parameterName;
        this.levelName = levelName;
        this.forecastTime = forecastTime;
        this.originX = originX;
        this.originY = originY;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.noXPoints = noXPoints;
        this.noYPoints = noYPoints;
        this.data = data;
    }

    public String getParameterName() {
        return parameterName;
    }

    public String getLevelName() {
        return levelName;
    }

    public float getOriginX() {
        return originX;
    }

    public float getOriginY() {
        return originY;
    }

    public float getDeltaX() {
        return deltaX;
    }

    public float getDeltaY() {
        return deltaY;
    }

    public int getNoXPoints() {
        return noXPoints;
    }

    public int getNoYPoints() {
        return noYPoints;
    }

    public float[] getData() {
        return data;
    }

    public ZonedDateTime getForecastTime() {
        return forecastTime;
    }
}
