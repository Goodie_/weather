package org.goodiemania.weather.internal;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ReportDateGenerator {
    private ZoneId UTC_ZONE;

    public ReportDateGenerator(final ZoneId UTC_ZONE) {
        this.UTC_ZONE = UTC_ZONE;
    }

    public ZonedDateTime get() {
        ZonedDateTime now = ZonedDateTime.now(UTC_ZONE)
                .withMinute(0)
                .withSecond(0)
                .withNano(0)
                .minusHours(4);

        //TODO what if the next forecast hasn't been published yet?
        if (now.getHour() < 6) {
            return now.withHour(0);
        } else if (now.getHour() < 12) {
            return now.withHour(6);
        } else if (now.getHour() < 18) {
            return now.withHour(12);
        } else {
            return now.withHour(18);
        }
    }
}
