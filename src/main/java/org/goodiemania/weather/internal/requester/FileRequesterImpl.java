package org.goodiemania.weather.internal.requester;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import org.apache.commons.io.IOUtils;
import org.goodiemania.weather.http.HttpServiceResponse;
import org.goodiemania.weather.http.impl.HttpRequestServiceImpl;

public class FileRequesterImpl implements FileRequester {
    private final static String GFS_URL_FORMAT = "https://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/gfs.%s/%s/gfs.t%sz.sfluxgrbf%s.grib2";
    private HttpRequestServiceImpl httpRequestService;

    public FileRequesterImpl(final HttpRequestServiceImpl httpRequestService) {
        this.httpRequestService = httpRequestService;
    }

    @Override
    public FileRequestResponse request(ZonedDateTime requestTime, ZonedDateTime forecastTime) {
        long forecastHours = ChronoUnit.HOURS.between(requestTime, forecastTime);

        String hour = String.format("%02d", requestTime.getHour());
        String forecastString = String.format("%03d", forecastHours);
        String dateString = requestTime.format(DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH));

        String url = String.format(GFS_URL_FORMAT, dateString, hour, hour, forecastString);
        System.out.println("Making request to: " + url);

        HttpServiceResponse httpServiceResponse = httpRequestService.get(url);

        try {
            File tempFile = File.createTempFile("grib", null);
            IOUtils.copy(httpServiceResponse.getResponse(), new FileOutputStream(tempFile));

            return new FileRequestResponse(tempFile, forecastTime);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
