package org.goodiemania.weather.internal.requester;

import java.io.File;
import java.time.ZonedDateTime;

public class FileRequestResponse {
    private File file;
    private ZonedDateTime forecastTime;

    public FileRequestResponse(final File file, final ZonedDateTime forecastTime) {
        this.file = file;
        this.forecastTime = forecastTime;
    }

    public FileRequestResponse() {
    }

    public File getFile() {
        return file;
    }

    public void setFile(final File file) {
        this.file = file;
    }

    public ZonedDateTime getForecastTime() {
        return forecastTime;
    }

    public void setForecastTime(final ZonedDateTime forecastTime) {
        this.forecastTime = forecastTime;
    }
}
