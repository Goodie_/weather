package org.goodiemania.weather.internal.requester;

import java.time.ZonedDateTime;

public interface FileRequester {
    FileRequestResponse request(ZonedDateTime requestTime, ZonedDateTime forecastTime);
}
