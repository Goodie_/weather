package org.goodiemania.weather.internal.requester;

import java.io.File;
import java.time.ZonedDateTime;

public class MockLocalFile implements FileRequester {
    private static final File SAMPLE_FILE = new File("C:\\Users\\Thomas Goodwin\\Downloads\\grib17867009251316047847.tmp");

    @Override
    public FileRequestResponse request(final ZonedDateTime requestTime, final ZonedDateTime forecastTime) {
        return new FileRequestResponse(SAMPLE_FILE, forecastTime);
    }
}
