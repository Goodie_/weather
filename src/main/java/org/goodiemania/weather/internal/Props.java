package org.goodiemania.weather.internal;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public enum Props {
    DATABASE_CONNECTION("DATABASE_CONNECTION"),
    DATABASE_USER("DATABASE_USER"),
    DATABASE_PASSWORD("DATABASE_PASSWORD");

    private final String prop;

    public final String get() {
        Properties properties = new Properties();
        String propertiesFile = System.getProperty("props", "/opt/deufault.properties");

        try {
            FileReader fileReader = new FileReader(propertiesFile);
            properties.load(fileReader);
        } catch (IOException e) {
            throw new IllegalStateException("Oh bmoy", e);
        }

        return properties.getProperty(prop);
    }

    Props(final String prop) {
        this.prop = prop;
    }
}
