package org.goodiemania.weather.internal;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.weather.database.Datasource;
import org.goodiemania.weather.domain.ForecastStorage;
import org.goodiemania.weather.internal.forecast.ForecastData;
import org.goodiemania.weather.internal.forecast.RecordData;
import org.goodiemania.weather.internal.forecast.RecordSelection;

public class WeatherProcessor {
    private Datasource datasource;

    public WeatherProcessor(
            final Datasource datasource) {
        this.datasource = datasource;
    }

    public void store(final ForecastData forecastData) {
        long startTime = System.currentTimeMillis();
        int p = 0;
        List<ForecastStorage> listToBeSaved = new ArrayList<>();
        for (int y = 0; y < forecastData.getNoXPoints(); y++) {
            for (int x = 0; x < forecastData.getNoYPoints(); x++, p++) {
                float longitude = forecastData.getOriginX() + (x * forecastData.getDeltaX());
                float latitude = forecastData.getOriginY() + (y * forecastData.getDeltaY());

                if (findData(p, RecordSelection.LOW_CLOUD_LAYER, forecastData.getData()) == 100
                        && findData(p, RecordSelection.MIDDLE_CLOUD_LAYER, forecastData.getData()) == 2
                        && findData(p, RecordSelection.HIGH_CLOUD_LAYER, forecastData.getData()) == 92) {
                    System.out.println(longitude + ":" + latitude + ";" + forecastData.getForecastTime());
                }

                if (latitude < -40 && latitude > -45
                        && longitude > 173 && longitude < 178) {
                    listToBeSaved.add(new ForecastStorage(
                            latitude,
                            longitude,
                            forecastData.getForecastTime(),
                            findData(p, RecordSelection.LOW_CLOUD_LAYER, forecastData.getData()),
                            findData(p, RecordSelection.MIDDLE_CLOUD_LAYER, forecastData.getData()),
                            findData(p, RecordSelection.HIGH_CLOUD_LAYER, forecastData.getData()),
                            findData(p, RecordSelection.ENTIRE_ATMOSPHERE, forecastData.getData()),
                            findData(p, RecordSelection.CONNECTIVE_CLOUD_LAYER, forecastData.getData()),
                            findData(p, RecordSelection.BOUNDARY_CLOUD_LAYER, forecastData.getData()),
                            findData(p, RecordSelection.LAND_COVER, forecastData.getData()),
                            findData(p, RecordSelection.ICE_COVER, forecastData.getData())
                    ));
                }
            }

            if (listToBeSaved.size() > 35000) {
                datasource.save(listToBeSaved);
                listToBeSaved = new ArrayList<>();
            }
        }

        if (!listToBeSaved.isEmpty()) {
            datasource.save(listToBeSaved);
        }

        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);  //divide by 1000000 to get milliseconds.
        System.out.println("WeatherProcessor.store() took: " + duration);
    }

    private Float findData(final int i, final RecordSelection selection, final List<RecordData> data) {
        return data.stream()
                .filter(recordData -> StringUtils.equals(selection.getParameter(), recordData.getParameter())
                        && StringUtils.equals(selection.getLevel(), recordData.getLevel()))
                .findFirst()
                .map(recordData -> recordData.getData()[i])
                .orElseGet(() -> {
                    System.out.println("Null value returned by getData()");
                    return null;
                });
    }

    public void letsMakeImage(final ForecastData forecastData) {
        RecordData recordData = forecastData.getData().stream()
                .filter(data -> StringUtils.equals(RecordSelection.LAND_COVER.getParameter(), data.getParameter())
                        && StringUtils.equals(RecordSelection.LAND_COVER.getLevel(), data.getLevel()))
                .findFirst()
                .orElseThrow();

        int noXpoints = forecastData.getNoXPoints();
        int noYpoints = forecastData.getNoYPoints();
        int p = 0;

        BufferedImage img = new BufferedImage(noXpoints, noYpoints, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < noYpoints; y++) {
            for (int x = 0; x < noXpoints; x++, p++) {
                float datum = recordData.getData()[p];

                BigDecimal longitude = BigDecimal.valueOf(forecastData.getOriginX() + (x * forecastData.getDeltaX()))
                        .setScale(2, RoundingMode.CEILING);
                BigDecimal latitude = BigDecimal.valueOf(forecastData.getOriginY() + (y * forecastData.getDeltaY()))
                        .setScale(2, RoundingMode.CEILING);

                if (compareLongLat(latitude, longitude,
                        BigDecimal.valueOf(-41.29), BigDecimal.valueOf(174.78))) {
                    int r = 25;
                    int g = 25;
                    int b = 255;
                    int col = (r << 16) | (g << 8) | b;

                    img.setRGB(x, y, col);
                } else {
                    int r = (int) (255 * datum);
                    int g = 25;
                    int b = 25;
                    int col = (r << 16) | (g << 8) | b;

                    img.setRGB(x, y, col);
                }

            }
        }

        try {
            ImageIO.write(img, "PNG", new File("stuff.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean compareLongLat(final BigDecimal long1, final BigDecimal lat1,
                                   final BigDecimal long2, final BigDecimal lat2) {
        return long1.setScale(1, RoundingMode.CEILING)
                .equals(long2.setScale(1, RoundingMode.CEILING))
                && lat1.setScale(1, RoundingMode.CEILING)
                .equals(lat2.setScale(1, RoundingMode.CEILING));
    }
}