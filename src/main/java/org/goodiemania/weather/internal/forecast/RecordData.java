package org.goodiemania.weather.internal.forecast;

public class RecordData {
    private String parameter;
    private String level;
    private float[] data;

    public RecordData() {
    }

    public static RecordData of(final String parameter, final String level, final float[] data) {
        RecordData recordData = new RecordData();
        recordData.parameter = parameter;
        recordData.level = level;
        recordData.data = data;

        return recordData;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(final String parameter) {
        this.parameter = parameter;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(final String level) {
        this.level = level;
    }

    public float[] getData() {
        return data;
    }

    public void setData(final float[] data) {
        this.data = data;
    }
}
