package org.goodiemania.weather.internal.forecast;

import java.time.ZonedDateTime;
import java.util.List;

public class ForecastData {
    private ZonedDateTime forecastTime;
    private float originX;
    private float originY;
    private float deltaX;
    private float deltaY;
    private int noXPoints;
    private int noYPoints;
    private List<RecordData> data;

    public ForecastData() {
    }

    public ForecastData(
            final ZonedDateTime forecastTime,
            final float originX,
            final float originY,
            final float deltaX,
            final float deltaY,
            final int noXPoints,
            final int noYPoints,
            final List<RecordData> data) {
        this.forecastTime = forecastTime;
        this.originX = originX;
        this.originY = originY;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.noXPoints = noXPoints;
        this.noYPoints = noYPoints;
        this.data = data;
    }

    public ZonedDateTime getForecastTime() {
        return forecastTime;
    }

    public void setForecastTime(final ZonedDateTime forecastTime) {
        this.forecastTime = forecastTime;
    }

    public float getOriginX() {
        return originX;
    }

    public void setOriginX(final float originX) {
        this.originX = originX;
    }

    public float getOriginY() {
        return originY;
    }

    public void setOriginY(final float originY) {
        this.originY = originY;
    }

    public float getDeltaX() {
        return deltaX;
    }

    public void setDeltaX(final float deltaX) {
        this.deltaX = deltaX;
    }

    public float getDeltaY() {
        return deltaY;
    }

    public void setDeltaY(final float deltaY) {
        this.deltaY = deltaY;
    }

    public int getNoXPoints() {
        return noXPoints;
    }

    public void setNoXPoints(final int noXPoints) {
        this.noXPoints = noXPoints;
    }

    public int getNoYPoints() {
        return noYPoints;
    }

    public void setNoYPoints(final int noYPoints) {
        this.noYPoints = noYPoints;
    }

    public List<RecordData> getData() {
        return data;
    }

    public void setData(final List<RecordData> data) {
        this.data = data;
    }
}
