package org.goodiemania.weather.internal.forecast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.weather.internal.requester.FileRequestResponse;
import ucar.grib.grib2.Grib2Data;
import ucar.grib.grib2.Grib2IndicatorSection;
import ucar.grib.grib2.Grib2Input;
import ucar.grib.grib2.Grib2Pds;
import ucar.grib.grib2.Grib2Record;
import ucar.grib.grib2.Grib2Tables;
import ucar.grib.grib2.ParameterTable;
import ucar.unidata.io.RandomAccessFile;

public class GFSService {
    private Set<RecordSelection> selections;

    public GFSService(final Set<RecordSelection> selections) {
        this.selections = selections;
    }

    public Optional<ForecastData> processFile(final FileRequestResponse fileRequestResponse) {
        long startTime = System.currentTimeMillis();
        try {
            if(!fileRequestResponse.getFile().exists()) {
                throw new IllegalStateException("Fuck. File does not exist.");
            }
            RandomAccessFile randomAccessFile = new RandomAccessFile(fileRequestResponse.getFile().getAbsolutePath(), "rw");
            randomAccessFile.order(0);
            Grib2Input input = new Grib2Input(randomAccessFile);
            Grib2Data gd = new Grib2Data(randomAccessFile);
            input.scan(false, false);

            if(input.getRecords().isEmpty()) {
                throw new IllegalStateException("File has no records");
            }

            List<Grib2Record> recordList = input.getRecords()
                    .stream()
                    .filter(this::isRecordValid)
                    .collect(Collectors.toList());

            if (recordList.isEmpty()) {
                return Optional.empty();
            }

            float dy = recordList.get(0).getGDS().getGdsVars().getDy() == -9999.0F ?
                    recordList.get(0).getGDS().getGdsVars().getDx() * -1 : recordList.get(0).getGDS().getGdsVars().getDy(); // latitude
            float dx = recordList.get(0).getGDS().getGdsVars().getDx(); // longitude

            ForecastData forecastData = new ForecastData(
                    fileRequestResponse.getForecastTime(),
                    recordList.get(0).getGDS().getGdsVars().getLo1(),
                    recordList.get(0).getGDS().getGdsVars().getLa1(),
                    dx, dy,
                    recordList.get(0).getGDS().getGdsVars().getNx(),
                    recordList.get(0).getGDS().getGdsVars().getNy(),
                    new ArrayList<>()
            );

            recordList.forEach(record -> {
                try {
                    Grib2IndicatorSection is = record.getIs();
                    Grib2Pds pdsv = record.getPDS().getPdsVars();
                    String parameterName = ParameterTable.getParameterName(is.getDiscipline(), pdsv.getParameterCategory(), pdsv.getParameterNumber());
                    String levelType = Grib2Tables.codeTable4_5(pdsv.getLevelType1());
                    float[] data = gd.getData(record.getGdsOffset(), record.getPdsOffset(), record.getId().getRefTime());

                    //TODO expand to full validation that it matches the above values
                    if (record.getGDS().getGdsVars().getScanMode() != 0) {
                        throw new IllegalStateException("Unknown scan mode found");
                    }

                    forecastData.getData().add(RecordData.of(parameterName, levelType, data));
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            });

            fileRequestResponse.getFile().delete();

            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);  //divide by 1000000 to get milliseconds.
            System.out.println("GFSService.processFile() took: " + duration);
            return Optional.of(forecastData);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private boolean isRecordValid(final Grib2Record record) {
        Grib2IndicatorSection is = record.getIs();
        Grib2Pds pdsv = record.getPDS().getPdsVars();

        String levelType = Grib2Tables.codeTable4_5(pdsv.getLevelType1());
        String parameterName = ParameterTable.getParameterName(is.getDiscipline(), pdsv.getParameterCategory(), pdsv.getParameterNumber());

        return selections.stream().anyMatch(recordSelection ->
                StringUtils.equals(recordSelection.getLevel(), levelType)
                        && StringUtils.equals(recordSelection.getParameter(), parameterName));
    }
}