package org.goodiemania.weather.internal.forecast;

public enum RecordSelection {
    LOW_CLOUD_LAYER("Total_cloud_cover","Low cloud layer"),
    MIDDLE_CLOUD_LAYER("Total_cloud_cover","Middle cloud layer"),
    HIGH_CLOUD_LAYER("Total_cloud_cover","High cloud layer"),
    ENTIRE_ATMOSPHERE("Total_cloud_cover","Entire Atmosphere"),
    CONNECTIVE_CLOUD_LAYER("Total_cloud_cover","Convective cloud layer"),
    BOUNDARY_CLOUD_LAYER("Total_cloud_cover","Boundary layer cloud layer"),
    LAND_COVER("Land_cover","Ground or water surface"),
    ICE_COVER("Ice_cover","Ground or water surface");


    private final String parameter;
    private final String level;

    RecordSelection(final String parameter, final String level) {
        this.parameter = parameter;
        this.level = level;
    }

    public String getParameter() {
        return parameter;
    }

    public String getLevel() {
        return level;
    }
}
