Some useful links
http://www.nco.ncep.noaa.gov/pmb/products/gfs/
Links to HTTP/FTP locations, as well as inventoryies

We are interested in GFS - T1534 Semi-Lagrangian grid,
http://www.nco.ncep.noaa.gov/pmb/products/gfs/gfs.t00z.pgrb2full.0p50.f000.shtml
http://www.nco.ncep.noaa.gov/pmb/products/gfs/gfs.t00z.pgrb2full.0p50.f006.shtml

These feature cloud levels at various levels. Yay.


Geopotential_height:::::::::::Hybrid level
Temperature:::::::::::Hybrid level
Specific_humidity:::::::::::Hybrid level
U-component_of_wind:::::::::::Hybrid level
V-component_of_wind:::::::::::Hybrid level
Pressure:::::::::::Ground or water surface
Geopotential_height:::::::::::Ground or water surface
Temperature:::::::::::Ground or water surface
Soil_temperature:::::::::::Depth below land surface
Volumetric_Soil_Moisture_Content:::::::::::Depth below land surface
Liquid_Volumetric_Soil_Moisture:::::::::::Depth below land surface
Soil_temperature:::::::::::Depth below land surface
Volumetric_Soil_Moisture_Content:::::::::::Depth below land surface
Liquid_Volumetric_Soil_Moisture:::::::::::Depth below land surface
Soil_temperature:::::::::::Depth below land surface
Volumetric_Soil_Moisture_Content:::::::::::Depth below land surface
Liquid_Volumetric_Soil_Moisture:::::::::::Depth below land surface
Soil_temperature:::::::::::Depth below land surface
Volumetric_Soil_Moisture_Content:::::::::::Depth below land surface
Liquid_Volumetric_Soil_Moisture:::::::::::Depth below land surface
Soil_moisture_content:::::::::::Depth below land surface
Plant_Canopy_Surface_Water:::::::::::Ground or water surface
Water_equivalent_of_accumulated_snow_depth:::::::::::Ground or water surface
Snow_Cover:::::::::::Ground or water surface
Snow_depth:::::::::::Ground or water surface
Potential_Evaporation_Rate:::::::::::Ground or water surface
Ice_thickness:::::::::::Ground or water surface
Aerodynamic_conductance:::::::::::Ground or water surface
Canopy_water_evaporation:::::::::::Ground or water surface
Direct_evaporation_from_bare_soil:::::::::::Ground or water surface
Transpiration:::::::::::Ground or water surface
Sublimation_evaporation_from_snow:::::::::::Ground or water surface
Temperature:::::::::::Specified height level above ground
Specific_humidity:::::::::::Specified height level above ground
Maximum_temperature:::::::::::Specified height level above ground
Minimum_temperature:::::::::::Specified height level above ground
Maximum_specific_humidity_at_2m:::::::::::Specified height level above ground
Minimum_specific_humidity_at_2m:::::::::::Specified height level above ground
U-component_of_wind:::::::::::Specified height level above ground
V-component_of_wind:::::::::::Specified height level above ground
Percent_frozen_precipitation:::::::::::Ground or water surface
Convective_Precipitation_Rate:::::::::::Ground or water surface
Precipitation_rate:::::::::::Ground or water surface
Storm_surface_runoff:::::::::::Ground or water surface
Water_runoff:::::::::::Ground or water surface
Latent_heat_net_flux:::::::::::Ground or water surface
Sensible_heat_net_flux:::::::::::Ground or water surface
Ground_Heat_Flux:::::::::::Ground or water surface
Snow_phase_change_heat_flux:::::::::::Ground or water surface
Momentum_flux_u_component:::::::::::Ground or water surface
Momentum_flux_v_component:::::::::::Ground or water surface
Surface_roughness:::::::::::Ground or water surface
Frictional_Velocity:::::::::::Ground or water surface
Zonal_Flux_of_Gravity_Wave_Stress:::::::::::Ground or water surface
Meridional_Flux_of_Gravity_Wave_Stress:::::::::::Ground or water surface
Sensible_heat_net_flux:::::::::::Ground or water surface
Latent_heat_net_flux:::::::::::Ground or water surface
Exchange_Coefficient:::::::::::Ground or water surface
Vegetation:::::::::::Ground or water surface
Ground_Heat_Flux:::::::::::Ground or water surface
Vegetation_Type:::::::::::Ground or water surface
Soil_type:::::::::::Ground or water surface
Surface_Slope_Type:::::::::::Ground or water surface
Wilting_Point:::::::::::Ground or water surface
Field_capacity:::::::::::Ground or water surface
SunShine_duration:::::::::::Ground or water surface
Potential_Evaporation_Rate:::::::::::Ground or water surface
Surface_Lifted_Index:::::::::::Ground or water surface
Precipitable_water:::::::::::Entire atmosphere layer
Total_cloud_cover:::::::::::Low cloud layer
Total_cloud_cover:::::::::::Middle cloud layer
Total_cloud_cover:::::::::::High cloud layer
Total_cloud_cover:::::::::::Entire Atmosphere
Pressure:::::::::::Convective cloud bottom level
Pressure:::::::::::Low cloud bottom level
Pressure:::::::::::Middle cloud bottom level
Pressure:::::::::::High cloud bottom level
Pressure:::::::::::Convective cloud top level
Pressure:::::::::::Low cloud top level
Pressure:::::::::::Middle cloud top level
Pressure:::::::::::High cloud top level
Temperature:::::::::::Low cloud top level
Temperature:::::::::::Middle cloud top level
Temperature:::::::::::High cloud top level
Total_cloud_cover:::::::::::Convective cloud layer
Total_cloud_cover:::::::::::Boundary layer cloud layer
Cloud_Work_Function:::::::::::Entire atmosphere layer
Downward_short_wave_rad_flux:::::::::::Ground or water surface
UV_B_downward_solar_flux:::::::::::Ground or water surface
Clear_sky_UV_B_downward_solar_flux:::::::::::Ground or water surface
Downward_long_wave_rad_flux:::::::::::Ground or water surface
Upward_short_wave_rad_flux:::::::::::Ground or water surface
Upward_long_wave_rad_flux:::::::::::Ground or water surface
Upward_short_wave_rad_flux:::::::::::Nominal top of the atmosphere
Upward_long_wave_rad_flux:::::::::::Nominal top of the atmosphere
Downward_short_wave_rad_flux:::::::::::Ground or water surface
Downward_long_wave_rad_flux:::::::::::Ground or water surface
Upward_short_wave_rad_flux:::::::::::Ground or water surface
Upward_long_wave_rad_flux:::::::::::Ground or water surface
Clear_sky_downward_solar_flux:::::::::::Ground or water surface
Clear_sky_upward_solar_flux:::::::::::Ground or water surface
Clear_sky_upward_solar_flux:::::::::::Nominal top of the atmosphere
Downward_short_wave_rad_flux:::::::::::Nominal top of the atmosphere
Clear_sky_downward_long_wave_flux:::::::::::Ground or water surface
Clear_sky_upward_long_wave_flux:::::::::::Ground or water surface
Clear_sky_upward_long_wave_flux:::::::::::Nominal top of the atmosphere
Visible_beam_downward_solar_flux:::::::::::Ground or water surface
Visible_diffuse_downward_solar_flux:::::::::::Ground or water surface
Near_IR_beam_downward_solar_flux:::::::::::Ground or water surface
Near_IR_diffuse_downward_solar_flux:::::::::::Ground or water surface
Planetary_Boundary_Layer_Height:::::::::::Ground or water surface
Land_cover:::::::::::Ground or water surface
Ice_cover:::::::::::Ground or water surface
Albedo:::::::::::Ground or water surface